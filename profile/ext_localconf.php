<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Eduard.Profile',
            'Userprofile',
            [
                'Profile' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Profile' => 'create, update, delete'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        userprofile {
                            iconIdentifier = profile-plugin-userprofile
                            title = LLL:EXT:profile/Resources/Private/Language/locallang_db.xlf:tx_profile_userprofile.name
                            description = LLL:EXT:profile/Resources/Private/Language/locallang_db.xlf:tx_profile_userprofile.description
                            tt_content_defValues {
                                CType = list
                                list_type = profile_userprofile
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'profile-plugin-userprofile',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:profile/Resources/Public/Icons/user_plugin_userprofile.svg']
			);
		
    }
);
