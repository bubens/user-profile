<?php
namespace Eduard\Profile\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Eduard Bubenshchikov 
 */
class ProfileControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Eduard\Profile\Controller\ProfileController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Eduard\Profile\Controller\ProfileController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllProfilesFromRepositoryAndAssignsThemToView()
    {

        $allProfiles = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $profileRepository = $this->getMockBuilder(\Eduard\Profile\Domain\Repository\ProfileRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $profileRepository->expects(self::once())->method('findAll')->will(self::returnValue($allProfiles));
        $this->inject($this->subject, 'profileRepository', $profileRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('profiles', $allProfiles);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenProfileToView()
    {
        $profile = new \Eduard\Profile\Domain\Model\Profile();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('profile', $profile);

        $this->subject->showAction($profile);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenProfileToProfileRepository()
    {
        $profile = new \Eduard\Profile\Domain\Model\Profile();

        $profileRepository = $this->getMockBuilder(\Eduard\Profile\Domain\Repository\ProfileRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $profileRepository->expects(self::once())->method('add')->with($profile);
        $this->inject($this->subject, 'profileRepository', $profileRepository);

        $this->subject->createAction($profile);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenProfileToView()
    {
        $profile = new \Eduard\Profile\Domain\Model\Profile();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('profile', $profile);

        $this->subject->editAction($profile);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenProfileInProfileRepository()
    {
        $profile = new \Eduard\Profile\Domain\Model\Profile();

        $profileRepository = $this->getMockBuilder(\Eduard\Profile\Domain\Repository\ProfileRepository::class)
            ->setMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $profileRepository->expects(self::once())->method('update')->with($profile);
        $this->inject($this->subject, 'profileRepository', $profileRepository);

        $this->subject->updateAction($profile);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenProfileFromProfileRepository()
    {
        $profile = new \Eduard\Profile\Domain\Model\Profile();

        $profileRepository = $this->getMockBuilder(\Eduard\Profile\Domain\Repository\ProfileRepository::class)
            ->setMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $profileRepository->expects(self::once())->method('remove')->with($profile);
        $this->inject($this->subject, 'profileRepository', $profileRepository);

        $this->subject->deleteAction($profile);
    }
}
