<?php
namespace Eduard\Profile\Domain\Model;


/***
 *
 * This file is part of the "User Profile Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Eduard Bubenshchikov
 *
 ***/
/**
 * Profile
 */
class Profile extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * firstName
     * 
     * @var string
     */
    protected $firstName = '';

    /**
     * email
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $email = '';

    /**
     * profileImage
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $profileImage = null;

    /**
     * password
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $password = '';

    /**
     * password2
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $password2 = '';

    /**
     * birthDate
     * 
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $birthDate = null;

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the firstName
     * 
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     * 
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the email
     * 
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     * 
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the profileImage
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImage
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Sets the profileImage
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImage
     * @return void
     */
    public function setProfileImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImage)
    {
        $this->profileImage = $profileImage;
    }

    /**
     * Returns the password
     * 
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the password
     * 
     * @param string $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the password2
     * 
     * @return string $password2
     */
    public function getPassword2()
    {
        return $this->password2;
    }

    /**
     * Sets the password2
     * 
     * @param string $password2
     * @return void
     */
    public function setPassword2($password2)
    {
        $this->password2 = $password2;
    }

    /**
     * Returns the birthDate
     * 
     * @return \DateTime $birthDate
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Sets the birthDate
     * 
     * @param \DateTime $birthDate
     * @return void
     */
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate = $birthDate;
    }
}
