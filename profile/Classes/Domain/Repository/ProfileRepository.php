<?php
namespace Eduard\Profile\Domain\Repository;


/***
 *
 * This file is part of the "User Profile Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Eduard Bubenshchikov
 *
 ***/
/**
 * The repository for Profiles
 */
class ProfileRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];
}
