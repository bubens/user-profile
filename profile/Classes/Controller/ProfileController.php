<?php
namespace Eduard\Profile\Controller;


/***
 *
 * This file is part of the "User Profile Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Eduard Bubenshchikov
 *
 ***/
/**
 * ProfileController
 */
class ProfileController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * profileRepository
     * 
     * @var \Eduard\Profile\Domain\Repository\ProfileRepository
     */
    protected $profileRepository = null;

    /**
     * @param \Eduard\Profile\Domain\Repository\ProfileRepository $profileRepository
     */
    public function injectProfileRepository(\Eduard\Profile\Domain\Repository\ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $profiles = $this->profileRepository->findAll();
        $this->view->assign('profiles', $profiles);
    }

    /**
     * action show
     * 
     * @param \Eduard\Profile\Domain\Model\Profile $profile
     * @return void
     */
    public function showAction(\Eduard\Profile\Domain\Model\Profile $profile)
    {
        $this->view->assign('profile', $profile);
    }

    /**
     * action new
     * 
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * action create
     * 
     * @param \Eduard\Profile\Domain\Model\Profile $newProfile
     * @return void
     */
    public function createAction(\Eduard\Profile\Domain\Model\Profile $newProfile)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profileRepository->add($newProfile);
        $this->redirect('list');
    }

    /**
     * action edit
     * 
     * @param \Eduard\Profile\Domain\Model\Profile $profile
     * @ignorevalidation $profile
     * @return void
     */
    public function editAction(\Eduard\Profile\Domain\Model\Profile $profile)
    {
        $this->view->assign('profile', $profile);
    }

    /**
     * action update
     * 
     * @param \Eduard\Profile\Domain\Model\Profile $profile
     * @return void
     */
    public function updateAction(\Eduard\Profile\Domain\Model\Profile $profile)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profileRepository->update($profile);
        $this->redirect('list');
    }

    /**
     * action delete
     * 
     * @param \Eduard\Profile\Domain\Model\Profile $profile
     * @return void
     */
    public function deleteAction(\Eduard\Profile\Domain\Model\Profile $profile)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profileRepository->remove($profile);
        $this->redirect('list');
    }
}
