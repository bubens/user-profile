#
# Table structure for table 'tx_profile_domain_model_profile'
#
CREATE TABLE tx_profile_domain_model_profile (

	name varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	profile_image int(11) unsigned NOT NULL default '0',
	password varchar(255) DEFAULT '' NOT NULL,
	password2 varchar(255) DEFAULT '' NOT NULL,
	birth_date date DEFAULT NULL

);
