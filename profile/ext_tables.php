<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Eduard.Profile',
            'Userprofile',
            'User Profile'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Eduard.Profile',
                'web', // Make module a submodule of 'web'
                'backenduserprofile', // Submodule key
                '', // Position
                [
                    'Profile' => 'list, show, new, create, edit, update, delete',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:profile/Resources/Public/Icons/user_mod_backenduserprofile.svg',
                    'labels' => 'LLL:EXT:profile/Resources/Private/Language/locallang_backenduserprofile.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('profile', 'Configuration/TypoScript', 'User Profile Extension');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_profile_domain_model_profile', 'EXT:profile/Resources/Private/Language/locallang_csh_tx_profile_domain_model_profile.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_profile_domain_model_profile');

    }
);
